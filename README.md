# Final Project Alpha QA 28 - "WEare" Social Network

## **[Test plan](https://gitlab.com/mercuryteam1/telerik-academy-final-project/-/blob/master/Deliverables/Test%20Plan.md)**
## **[Test Cases](https://docs.google.com/spreadsheets/d/1l22C4bWnyq-a4dPQ5Br3R4TfWAbiJ3KyDRwIiZtJyJQ/edit#gid=0)** <br>
## **[Local setup before running the tests](https://gitlab.com/mercuryteam1/telerik-academy-final-project/-/blob/master/Deliverables/Prerequisites.md)**
## **[Selenium tests](https://gitlab.com/mercuryteam1/telerik-academy-final-project/-/tree/master/WEare%20Selenium%20tests)** <br>
## **[REST tests](https://gitlab.com/mercuryteam1/telerik-academy-final-project/-/tree/master/WeAre%20Rest%20tests)** <br>
## **[Trello Board](https://trello.com/b/Um4yWeTr/final-project)** <br>
## **[Final Report](https://drive.google.com/file/d/1dTzKAcUfQoof3H--1SnAT5A9zgm5jdgX/view)**
