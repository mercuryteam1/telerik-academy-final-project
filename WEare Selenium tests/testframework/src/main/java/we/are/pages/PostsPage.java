package we.are.pages;

import com.telerikacademy.testframework.Utils;
import com.telerikacademy.testframework.pages.BasePage;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class PostsPage extends BasePage {

    public PostsPage(WebDriver driver) { super(driver, "weAre.homePage");
    }

    public void selectValueFromDropDown(String value, String button, String locator) {
        actions.waitForElementVisible(button);
        Select dropdownObject = new Select(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
        dropdownObject.selectByVisibleText(Utils.getTestDataPropertyByKey(value));
        actions.clickElement(Utils.getUIMappingByKey(button));
    }

    public void inputCommentInField(String field, int length) {
        actions.waitForElementVisible("weAre.posts.createNewPostField");
        String randomText = RandomStringUtils.randomAlphabetic(length);
        actions.typeValueInField(randomText, Utils.getUIMappingByKey(field));
    }

    public void attachPicture(String chooseFileButton, String file) {
        WebElement addFile = driver.findElement(By.xpath(Utils.getUIMappingByKey(chooseFileButton)));
        addFile.sendKeys(file);
    }

    public void assertLatestPostsHeader() {
        actions.assertElementPresent("weAre.latestPosts.Header");
    }

}
