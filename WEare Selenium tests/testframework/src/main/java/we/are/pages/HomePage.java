package we.are.pages;

import com.telerikacademy.testframework.Utils;
import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class HomePage extends BasePage {


    public HomePage(WebDriver driver) {
        super(driver, "weAre.homePage");
    }


    public void clickButtonToOpenPage(String button) {
        actions.waitForElementVisible(button);
        actions.clickElement(button);
    }

    public void searchValueInField(String value, String field) {
        actions.waitForElementVisible(field, 10);
        actions.typeValueInField(Utils.getTestDataPropertyByKey(value), Utils.getUIMappingByKey(field));
    }

    public void searchValueInFieldBDD(String value, String field) {
        actions.waitForElementVisible(field, 10);
        actions.typeValueInField(value, Utils.getUIMappingByKey(field));
    }

    public int numberOfElements() {
        int count = 0;
        count = driver.findElements(By.xpath(Utils.getUIMappingByKey("weAre.count"))).size();
        return count;
    }



    public void clearField(String xpath) {
        driver.findElement(By.xpath(Utils.getUIMappingByKey(xpath))).clear();
    }

    public void assertUserName() {
        actions.assertElementPresent("weAre.assertFoundName");
    }

    public void assertUserProfession() {
        actions.waitForElementVisible("weAre.assert.actorLabel", 5);
        actions.assertElementPresent("weAre.assert.actorLabel");
    }

}
