package we.are.pages;

import com.telerikacademy.testframework.Utils;
import com.telerikacademy.testframework.pages.BasePage;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;

public class RegisterPage extends BasePage {

    public RegisterPage(WebDriver driver) {
        super(driver, "weAre.RegisterPage.Url");

    }

    public void registerNewUser(String name, String email, String password, String confirmPassword ){
        actions.waitForElementVisible("weAre.registrationUsernameField");
        actions.typeValueInField(Utils.getTestDataPropertyByKey(name), "weAre.registrationUsernameField");
        actions.typeValueInField(Utils.getTestDataPropertyByKey(email), "weAre.registrationEmailField");
        actions.typeValueInField(Utils.getTestDataPropertyByKey(password), "weAre.registrationPasswordField");
        actions.typeValueInField(Utils.getTestDataPropertyByKey(confirmPassword), "weAre.registrationPasswordConfirmField");
        actions.clickElement("weAre.registerButtonSubmit");
    }

    public void registerUserWithUniqueName(String email, String password, String confirmPassword){
        actions.waitForElementVisible("weAre.registrationUsernameField");
        String name = RandomStringUtils.randomAlphabetic(8);
        actions.typeValueInField(name, "weAre.registrationUsernameField");
        actions.typeValueInField(Utils.getTestDataPropertyByKey(email), "weAre.registrationEmailField");
        actions.typeValueInField(Utils.getTestDataPropertyByKey(password), "weAre.registrationPasswordField");
        actions.typeValueInField(Utils.getTestDataPropertyByKey(confirmPassword), "weAre.registrationPasswordConfirmField");
        actions.clickElement("weAre.registerButtonSubmit");
    }


}
