package we.are.pages;

import com.telerikacademy.testframework.Utils;
import com.telerikacademy.testframework.pages.BasePage;
import org.openqa.selenium.WebDriver;

public class LogInPage extends BasePage {

    public LogInPage(WebDriver driver) {
        super(driver, "weAre.LogIn.Url");
    }

    public void logInWithCredentials(String username, String password) {
//        actions.waitForElementVisible("weAre.logInPageAssert");
        actions.typeValueInField(Utils.getTestDataPropertyByKey(username), "weAre.signInUsernameField");
        actions.typeValueInField(Utils.getTestDataPropertyByKey(password), "weAre.signInPasswordField");
        actions.clickElement("weAre.logInButton");

    }

    public void logOut() {
        actions.assertElementPresent("weAre.logOutButton");
        actions.clickElement("weAre.logOutButton");
        actions.assertElementPresent("weAre.signInButton");
    }

}
