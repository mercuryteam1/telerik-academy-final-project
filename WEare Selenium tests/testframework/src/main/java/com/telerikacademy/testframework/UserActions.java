package com.telerikacademy.testframework;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserActions {
    public WebDriver getDriver() {
        return driver;
    }

    final WebDriver driver;

    public UserActions() {
        this.driver = Utils.getWebDriver();
    }

    public static void loadBrowser(String baseUrlKey) {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey(baseUrlKey));
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }

    public void clickElement(String key, Object... arguments) {
        String locator = getLocatorValueByKey(key, arguments);
        Utils.LOG.info("Clicking on element " + key);
        WebElement element = driver.findElement(By.xpath(locator));
        element.click();
    }

    public void hoverElement(String key, Object... arguments) {
        String locator = getLocatorValueByKey(key, arguments);
        Utils.LOG.info("Hover on element " + key);
        Actions action = new Actions(driver);
        WebElement element = driver.findElement(By.xpath(locator));
        action.moveToElement(element);

    }

    public void typeValueInField(String value, String field, Object... fieldArguments) {
        String locator = getLocatorValueByKey(field, fieldArguments);
        WebElement element = driver.findElement(By.xpath(locator));
        Utils.LOG.info("Type " + value + " in field " + field);
        element.sendKeys(value);
    }

    public void switchToIFrame(String iframe) {
        WebElement element = driver.findElement(By.id(iframe));
        Utils.LOG.info("Switch to " + iframe);
        driver.switchTo();
    }

    //############# WAITS #########

    public void waitForElementVisible(String locatorKey, Object... arguments) {
        int defaultTimeout = Integer.parseInt(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds"));
        waitForElementVisibleUntilTimeout(locatorKey, defaultTimeout, arguments);

    }

    public void waitForElementVisibleUntilTimeout(String locator, int seconds, Object... locatorArguments) {
        WebDriverWait webDriverWait = new WebDriverWait(driver, seconds);
        String elementLocator = getLocatorValueByKey(locator, locatorArguments);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementLocator)));
        webDriverWait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(locator)));
        Assert.assertTrue("Element is not visible!", isElementVisible(elementLocator));


    }

    public void waitForElementPresent(String locator, Object... arguments) {
        String defaultTimeOut = "config.defaultTimeoutSeconds";
        WebDriverWait webDriverWait = new WebDriverWait(driver, Integer.parseInt(defaultTimeOut));
        String elementLocator = getLocatorValueByKey(locator, arguments);
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementLocator)));
        Assert.assertTrue("Element with locator " + elementLocator +
                " is not found on the page!", isElementPresent(elementLocator));
    }

    public boolean isElementPresent(String locator, Object... arguments) {
        WebDriverWait webDriverWait = new WebDriverWait(driver, 10);
        String elementLocator = getLocatorValueByKey(locator, arguments);
        WebElement element = driver.findElement(By.xpath(elementLocator));
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementLocator)));
        return element.isDisplayed();


    }

    public boolean isElementVisible(String locator, Object... arguments) {
        int defaultTimeOut = Integer.parseInt(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds"));
        WebDriverWait webDriverWait = new WebDriverWait(driver, defaultTimeOut);
        WebElement element = driver.findElement(By.xpath(locator));
        webDriverWait.until(ExpectedConditions.visibilityOf(element));
        return element.isDisplayed();
    }

    public void waitFor(long timeOutMilliseconds) {
        try {
            Thread.sleep(timeOutMilliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //############# ASSERTS #########

    public void assertElementPresent(String locator) {
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    public void assertElementAttribute(String locator, String attributeName, String attrubuteValue) {
        WebElement element = driver.findElement(By.xpath(locator));
        String typeValue = element.getAttribute(attributeName);
        Assert.assertTrue("Element value is not equal to expected value!", typeValue.equals(attrubuteValue));
    }

    public void assertNavigatedUrl(String urlKey) {
        String currentUrl = driver.getCurrentUrl();
        Assert.assertEquals("Current URL doesn't match expected URL!", true, currentUrl.equals(urlKey));

    }

    public void pressKey(Keys key) {
        UserActions actions = new UserActions();
        actions.pressKey(key);
        Utils.LOG.info("Press key " + key);
    }

    private String getLocatorValueByKey(String locator, Object[] arguments) {
        return String.format(Utils.getUIMappingByKey(locator), arguments);
    }

    public void sendKeys(Keys arrowLeft) {
    }

    public void searchNameField(String s) {
    }

    public void searchProfessionField(String s) {
    }
}
