Meta:
@updatePersonalProfile

Narrative:
As a registered user in "WEare" social network
I want to update my profile, containing First Name, Last Name and a birthday date
So that I can keep my profile up-to-date

Scenario: Update user's personal profile information
Given The user is logged in and navigated to edit menu in personal profile
When The user updates profile personal information with Legolas, Greenleaf and 10122001
Then The newly added information is successfully saved

Scenario: The user is logged out after the personal profile is updated
Given The personal profile is updated
When The user is logged out
Then Log in button is visible