package stepDefinitions;

import com.telerikacademy.testframework.UserActions;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import we.are.pages.*;

public class StepDefinitions extends BaseStepDefinition{

    UserActions actions = new UserActions();
    HomePage homePage = new HomePage(actions.getDriver());
    LogInPage logInPage = new LogInPage(actions.getDriver());


    @Given("The user is logged in and navigated to edit menu in personal profile")
    public void logIn() {
        logInPage.navigateToPage();
        logInPage.logInWithCredentials("weAre.userName", "weAre.userPassword");
        homePage.clickButtonToOpenPage("weAre.profileButton");
        homePage.clickButtonToOpenPage("weAre.editProfileButton");
    }

    @When("The user updates profile personal information with $firstname, $lastname and $birthday")
    public void addPersonalInfo(String firstname, String lastname, String birthday) {
        homePage.clearField("weAre.firstNameField");
        homePage.searchValueInFieldBDD(firstname, "weAre.firstNameField");
        homePage.clearField("weAre.lastNameField");
        homePage.searchValueInFieldBDD(lastname, "weAre.lastNameField");
        homePage.searchValueInFieldBDD(birthday, "weAre.birthdayField");
        homePage.clickButtonToOpenPage("weAre.updateProfileButton");
    }

    @Then("The newly added information is successfully saved")
    public void successfulProfileUpdate () {
        Assert.assertTrue("Profile update failed", actions.isElementPresent("weAre.profile.assertName"));
    }

    @Given("The personal profile is updated")
    public void personalProfileUpdated() {
        Assert.assertTrue("Profile update failed", actions.isElementPresent("weAre.profile.assertName"));
    }

    @When("The user is logged out")
    public void logOut() {
        login.logOut();
    }

    @Then("Log in button is visible")
    public void logInButtonVisible() {

        Assert.assertTrue("Logout failed", actions.isElementPresent("weAre.logInButton"));
    }
}
