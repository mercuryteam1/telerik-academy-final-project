package stepDefinitions;

import com.telerikacademy.testframework.UserActions;
import org.jbehave.core.annotations.*;
import we.are.pages.HomePage;
import we.are.pages.LogInPage;

public class BaseStepDefinition {

    UserActions actions = new UserActions();
    HomePage home = new HomePage(actions.getDriver());
    LogInPage login = new LogInPage(actions.getDriver());

    @BeforeStory
    public void setUp() {
        UserActions.loadBrowser("base.url");
    }

    @AfterStory
    public void tearDown() {
        UserActions.quitDriver();
    }
}