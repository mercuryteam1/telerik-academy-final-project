package WeAre.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RegistrationTests extends BaseTest {

    @Before
    public void goToRegistrationPage() {
        registerPage.navigateToPage();
    }

    @Test
    public void Reg_1_User_ShouldBeAble_To_RegisterOnTheSite_When_NeededInformation_Is_Provided() {

       registerPage.registerUserWithUniqueName(
                "weAre.register.Email",
                "weAre.register.Pass",
                "weAre.register.Pass");

        actions.waitForElementVisible("weAre.registration.message");

        Assert.assertTrue("Registration message is not displayed",
                actions.isElementPresent("weAre.registration.message"));
    }

    @Test
    public void Reg_2_System_ShouldThrowProperException_When_Username_IsNotProvided() {
        registerPage.registerNewUser(
                "",
                "weAre.register.Email",
                "weAre.register.Pass",
                "weAre.register.Pass");

        actions.waitForElementVisible("weAre.unsuccessfulregistration");

        Assert.assertTrue("Exception message for missing username is not displayed",
                actions.isElementPresent("weAre.unsuccessfulregistration"));
    }

    @Test
    public void Reg_3_System_ShouldThrowProperException_When_Email_IsNotProvided() {
        registerPage.registerNewUser(
                "weAre.register.Name",
                "",
                "weAre.register.Pass",
                "weAre.register.Pass");

        actions.waitForElementVisible("weAre.unsuccessfulregistration");
        Assert.assertTrue("Exception message for missing email is not displayed",
                actions.isElementPresent("weAre.unsuccessfulregistration"));
    }

    @Test
    public void Reg_4_System_ShouldThrowProperException_When_Password_IsNotProvided() {
        registerPage.registerNewUser(
                "weAre.register.Name",
                "weAre.register.Email",
                "",
                "weAre.register.Pass");

        actions.waitForElementVisible("weAre.unsuccessfulregistration");

        Assert.assertTrue("Exception message for missing password is not displayed",
                actions.isElementPresent("weAre.unsuccessfulregistration"));
    }

    @Test
    public void Reg_5_System_ShouldThrowProperException_When_Password_IsNotConfirmed() {
        registerPage.registerNewUser(
                "weAre.register.Name",
                "weAre.register.Email",
                "weAre.register.Pass",
                "");

        actions.waitForElementVisible("weAre.unsuccessfulregistration");

        Assert.assertTrue("Exception message for not confirming the password is not displayed",
                actions.isElementPresent("weAre.unsuccessfulregistration"));
    }

    @Test
    public void Reg_6_System_ShouldThrowProperException_When_Email_IsNotValid() {
        registerPage.registerNewUser(
                "weAre.register.Name",
                "weAre.register.InvalidEmail",
                "weAre.register.Pass",
                "weAre.register.Pass");

        actions.waitForElementVisible("weAre.invalidEmail.errMsg");

        Assert.assertTrue("Exception message for invalid email is not displayed",
                actions.isElementPresent("weAre.invalidEmail.errMsg"));
    }

    @Test
    public void Reg_7_System_ShouldThrowProperException_When_RegisteringUserWithExistingUsername() {
        registerPage.registerNewUser(
                "weAre.register.Name",
                "weAre.register.Email",
                "weAre.register.Pass",
                "weAre.register.Pass");

        actions.waitForElementVisible("weAre.existingUser.errMsg");

        Assert.assertTrue("Exception message for existing username is not displayed",
                actions.isElementPresent("weAre.existingUser.errMsg"));
    }

    @Test
    public void Reg_8_System_ShouldThrowProperException_When_Password_IsSmallerThanSixSymbols() {
        registerPage.registerNewUser(
                "weAre.register.Name",
                "weAre.register.Email",
                "weAre.register.ShortPass",
                "weAre.register.ShortPass");

        actions.waitForElementVisible("weAre.shortPass.errorMsg");

        Assert.assertTrue("Exception message for short password is not displayed",
                actions.isElementPresent("weAre.shortPass.errorMsg"));
    }

    @Test
    public void Reg_9_System_ShouldThrowProperException_When_Password_IsNotConfirmedCorrectly() {
        registerPage.registerNewUser(
                "weAre.register.Name",
                "weAre.register.Email",
                "weAre.register.Pass",
                "weAre.register.WrongPassConfirmation");

        actions.waitForElementVisible("weAre.notConfirmedPass.errMsg");

        Assert.assertTrue("Exception message for not correctly confirming the is not displayed",
                actions.isElementPresent("weAre.notConfirmedPass.errMsg"));
    }


}
