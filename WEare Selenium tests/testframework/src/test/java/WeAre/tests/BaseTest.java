package WeAre.tests;

import com.telerikacademy.testframework.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import we.are.pages.*;

public class BaseTest {

    UserActions actions = new UserActions();
    HomePage homePage = new HomePage(actions.getDriver());
    RegisterPage registerPage = new RegisterPage(actions.getDriver());
    LogInPage logInPage = new LogInPage(actions.getDriver());
    PostsPage postsPage = new PostsPage(actions.getDriver());


    @BeforeClass
    public static void setUp() {
        UserActions.loadBrowser("base.url");
    }

    @AfterClass
    public static void tearDown() {
        UserActions.quitDriver();
    }
}
