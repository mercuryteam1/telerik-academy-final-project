package WeAre.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UnregisteredUserTests extends BaseTest {


    @Before
    public void navigateToThePage() {
        homePage.clickButtonToOpenPage("weAre.homePage.button");
    }

    @Test
    public void Unreg_User_1_SearchUserByProfession() {
        homePage.searchValueInField("weAre.userProfession", "weAre.searchField.byProfession");
        homePage.clickButtonToOpenPage("weAre.homePage.searchButton");
        homePage.assertUserProfession();
        Assert.assertTrue("Search by profession failed", actions.isElementPresent("weAre.assert.actorLabel"));
    }


    @Test
    public void Unreg_User_2_SearchUserByName() {
        homePage.searchValueInField("weAre.fourthUser.firstName", "weAre.searchField.byName");
        homePage.clickButtonToOpenPage("weAre.homePage.searchButton");
        homePage.assertUserName();
        Assert.assertTrue("Searched name not found", actions.isElementPresent("weAre.assertFoundName"));
    }

    @Test
    public void Unreg_User_3_ShowLatestPostsFromUsers() {
        homePage.clickButtonToOpenPage("weAre.latestPostsButton");
        Assert.assertTrue("No posts", actions.isElementPresent("weAre.publicPostLabel"));
    }

    @Test
    public void Unreg_User_4_ShowPostsCategoryByProfession() {
        homePage.clickButtonToOpenPage("weAre.latestPostsButton");
        postsPage.selectValueFromDropDown("weAre.userProfession",
                "weAre.latestPosts.browseButton", "weAre.latestPosts.categoryDropDown");
        Assert.assertTrue("Wrong category comments", actions.isElementPresent("weAre.assert.actorLabel"));
    }
}



