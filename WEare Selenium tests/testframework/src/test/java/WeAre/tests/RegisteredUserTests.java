package WeAre.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegisteredUserTests extends BaseTest{

    @Before
    public void navigateToThePage() {
        logInPage.navigateToPage();
        logInPage.logInWithCredentials("weAre.userName", "weAre.userPassword");
        Assert.assertTrue("Unsuccessful login", actions.isElementPresent("weAre.logOutButton"));
    }

    @Test
    public void Reg_User_01_AddNewPrivatePost_WithoutAttachedImage() {
        homePage.clickButtonToOpenPage("weAre.profile.addNewPostButton");
        postsPage.inputCommentInField("weAre.posts.createNewPostField", 20);
        postsPage.selectValueFromDropDown("weAre.posts.visibilityPrivate",
                "weAre.posts.savePostButton", "weAre.posts.postVisibilityDropDown");
        Assert.assertTrue("The post is not published", actions.isElementPresent("weAre.profile.newPostButton"));
    }

    @Test
    public void Reg_User_02_AddNewPublicPost_WithAttachedImage() {
        homePage.clickButtonToOpenPage("weAre.profile.addNewPostButton");
        postsPage.inputCommentInField("weAre.posts.createNewPostField", 20);
        postsPage.attachPicture("weAre.posts.chooseFileButton", "C:\\Users\\Andrey\\OneDrive\\Desktop");
        postsPage.selectValueFromDropDown("weAre.posts.visibilityPublic",
                "weAre.posts.savePostButton", "weAre.posts.postVisibilityDropDown");
        actions.waitForElementVisible("weAre.profile.newPostButton");
        Assert.assertTrue("The new post is not published", actions.isElementPresent("weAre.profile.newPostButton"));
    }

    @Test
    public void Reg_User_03_LeaveCommentToExistingPost() {
        homePage.clickButtonToOpenPage("weAre.latestPosts.Button");
        homePage.clickButtonToOpenPage("weAre.latestPosts.browseButton");
        homePage.clickButtonToOpenPage("weAre.latestPosts.ExploreThisPostButton");
        postsPage.inputCommentInField("weAre.posts.commentField", 20);
        homePage.clickButtonToOpenPage("weAre.posts.postCommentButton");
        actions.waitForElementVisible("weAre.posts.postCommentButton");
        Assert.assertTrue("The comment is not posted", actions.isElementPresent("weAre.posts.postCommentButton"));
    }

    @Test
    public void Reg_User_04_SendFriendRequest_ToAnotherRegisteredUser() {
        logInPage.logOut();
        logInPage.logInWithCredentials("weAre.fourthUser.firstName", "weAre.fourthUser.password");
        homePage.searchValueInField("weAre.firstName", "weAre.searchField.byName");
        homePage.clickButtonToOpenPage("weAre.homePage.searchButton");
        homePage.clickButtonToOpenPage("weAre.seeProfileButton");
        homePage.clickButtonToOpenPage("weAre.connectButton");
        Assert.assertTrue("You have failed to send friend request!", actions.isElementPresent("weAre.assert.connect"));
    }

    @Test
    public void Reg_User_05_AcceptFriendRequest_FromAnotherUser() {
        int countBefore = 0;
        int countAfter = 0;
        homePage.clickButtonToOpenPage("weAre.profileButton");
        homePage.clickButtonToOpenPage("weAre.profile.newFriendRequestsButton");
        actions.waitFor(100);
        countBefore = homePage.numberOfElements();
        homePage.clickButtonToOpenPage("weAre.profile.approveRequestButton");
        actions.waitFor(100);
        countAfter = homePage.numberOfElements();
        Assert.assertTrue("Friend request was not approved!", countBefore > countAfter);
    }

    @Test
    public void Reg_User_06_CreateEmptyPrivatePost() {
        homePage.clickButtonToOpenPage("weAre.profile.addNewPostButton");
        homePage.clickButtonToOpenPage("weAre.posts.createNewPostField");
        postsPage.selectValueFromDropDown("weAre.posts.visibilityPrivate", "weAre.posts.savePostButton",
                "weAre.posts.postVisibilityDropDown");
        Assert.assertTrue("This test has to fail!", actions.isElementPresent("weAre.latestPosts.Header"));
    }

    @Test
    public void Reg_User_07_CreateNewPrivatePost_WithSingleSymbolComment() {
        homePage.clickButtonToOpenPage("weAre.profile.addNewPostButton");
        postsPage.inputCommentInField("weAre.posts.createNewPostField", 1);
        postsPage.selectValueFromDropDown("weAre.posts.visibilityPrivate", "weAre.posts.savePostButton",
                "weAre.posts.postVisibilityDropDown");
        Assert.assertTrue("This test has to fail!", actions.isElementPresent("weAre.latestPosts.Header"));
    }

    @Test
    public void Reg_User_08_LikePublishedPost() {
        homePage.clickButtonToOpenPage("weAre.latestPostsButton");
        homePage.clickButtonToOpenPage("weAre.latestPosts.likeButton");
        Assert.assertTrue("Failed to like this post!", actions.isElementPresent("weAre.latestPosts.likeButton"));
    }

    @Test
    public void Reg_User_09_DeletePost_CreatedBySameUser() {
        homePage.clickButtonToOpenPage("weAre.profileButton");
        homePage.clickButtonToOpenPage("weAre.profile.latestActivityTab");
        homePage.clickButtonToOpenPage("weAre.profile.recentPosts");
        homePage.clickButtonToOpenPage("weAre.posts.deletePostButton");
        postsPage.selectValueFromDropDown("weAre.posts.deletePost", "weAre.posts.deleteSubmitButton",
                "weAre.posts.deleteDropDown");
        actions.waitForElementVisible("weAre.latestPosts.postDeleted");
        Assert.assertTrue("Failed to delete this post!", actions.isElementPresent("weAre.latestPosts.postDeleted"));
    }

    @Test
    public void Reg_User_10_EditPost_CreatedBySameUser() {
        homePage.clickButtonToOpenPage("weAre.profileButton");
        homePage.clickButtonToOpenPage("weAre.profile.latestActivityTab");
        homePage.clickButtonToOpenPage("weAre.profile.recentPosts");
        homePage.clickButtonToOpenPage("weAre.posts.editPostButton");
        postsPage.inputCommentInField("weAre.posts.commentField", 20);
        homePage.clickButtonToOpenPage("weAre.posts.savePostButton");
        actions.waitForElementVisible("weAre.assert.allPostsOfUserButton");
        Assert.assertTrue("Failed to edit this post!", actions.isElementPresent("weAre.assert.allPostsOfUserButton"));
    }

    @Test
    public void Reg_User_11_CreatePublicPost_WithTextMoreThan1000Symbols() {
        homePage.clickButtonToOpenPage("weAre.profile.addNewPostButton");
        postsPage.inputCommentInField("weAre.posts.createNewPostField", 1001);
        postsPage.selectValueFromDropDown("weAre.posts.visibilityPublic", "weAre.posts.savePostButton",
                "weAre.posts.postVisibilityDropDown");
        actions.waitForElementVisible("weAre.assert.contentSize");
        Assert.assertTrue("Error expected but not present", actions.isElementPresent("weAre.assert.contentSize"));
    }

    @Test
    public void Reg_User_12_CreatePublicPost_WithTextBetween600And1000Symbols() {
        homePage.clickButtonToOpenPage("weAre.profile.addNewPostButton");
        postsPage.inputCommentInField("weAre.posts.createNewPostField", 777);
        postsPage.selectValueFromDropDown("weAre.posts.visibilityPublic", "weAre.posts.savePostButton",
                "weAre.posts.postVisibilityDropDown");
        Assert.assertTrue("Failed to create this post", actions.isElementPresent("weAre.latestPosts.ExploreThisPostButton"));
    }

    @Test
    public void Reg_User_99_ResetConnectionRequest() {
        homePage.searchValueInField("weAre.fourthUser.firstName", "weAre.searchField.byName");
        homePage.clickButtonToOpenPage("weAre.homePage.searchButton");
        homePage.clickButtonToOpenPage("weAre.seeProfileButton");
        homePage.clickButtonToOpenPage("weAre.disconnectButton");
        Assert.assertTrue("You have failed to disconnect from the user!", actions.isElementPresent("weAre.connectButton"));
    }
}