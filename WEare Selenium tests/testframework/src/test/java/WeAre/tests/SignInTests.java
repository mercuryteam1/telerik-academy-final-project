package WeAre.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SignInTests extends BaseTest{

    @Before
    public void goToSignInPage() {
        logInPage.navigateToPage();
    }

    @Test
    public void SignIn_1_RegisteredUser_ShouldBeAble_To_SignIn() {
        logInPage.logInWithCredentials("weAre.userName", "weAre.userPassword");
        actions.waitForElementVisible("weAre.logOutButton");
        Assert.assertTrue("Sign in was not successful!",actions.isElementPresent("weAre.logOutButton"));
}
    @Test
    public void System_ShouldThrowProperException_When_UsernameField_isEmpty(){
        logInPage.logInWithCredentials("", "weAre.userPassword");
        actions.waitForElementVisible("weAre.LogIn.ErrorMessage");
        Assert.assertTrue("Error message is not displayed",actions.isElementPresent("weAre.LogIn.ErrorMessage") );
    }

    @Test
    public void SignIn_2_System_ShouldThrowProperException_When_User_IsTryingToSignIn_With_WrongUsername(){
        logInPage.logInWithCredentials("weAre.wrongUsername", "weAre.userPassword");
        actions.waitForElementVisible("weAre.LogIn.ErrorMessage");
        Assert.assertTrue("Error message is not displayed",actions.isElementPresent("weAre.LogIn.ErrorMessage") );
    }


    @Test
    public void System_ShouldThrowProperException_When_PasswordField_isEmpty(){
        logInPage.logInWithCredentials("weAre.userName", "");
        actions.waitForElementVisible("weAre.LogIn.ErrorMessage");
        Assert.assertTrue("Error message is not displayed",actions.isElementPresent("weAre.LogIn.ErrorMessage") );

    }

    @Test
    public void SignIn_3_System_ShouldThrowProperException_When_User_IsTryingToSignIn_With_WrongPassword(){
        logInPage.logInWithCredentials("weAre.userName", "weAre.wrongPassword");
        actions.waitForElementVisible("weAre.LogIn.ErrorMessage");
        Assert.assertTrue("Error message is not displayed",actions.isElementPresent("weAre.LogIn.ErrorMessage") );

    }





}
