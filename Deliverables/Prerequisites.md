# "WEare" website testing prerequisites
 

This section describes the tasks that you must complete and the requirements you should meet before running the "WeAre" website tests.

## Hardware requirements
1. One test computer running a supported operating system. This computer must include the following:
   - Windows keyboard
   - Two-button pointing device (this is optional if the test computer is a laptop with a touch pad or other input capability).
   - Color display monitor capable of at least 1024 by 768 resolution, 32-bits per pixel, 60 Hz.
    
## Software requirements
I. Before running the tests, you should have installed and running the following software on the test computer:  
1. One of the following operating systems:
- Windows 10;
- MacOS.
2. Java 11 and JDK 11.
   ####
3. Newman.
####
4. Maven with surefire plugin.
####
5. Selenium webdriver.
####
6. Browsers:
- Google Chrome;
- Mozila firefox.

II. The "WEare" website is deployed online and can be found [Here](http://164.138.216.247:8082/). 
If you want to run the website on your local machine follow the instructions:
## 1. Install Docker
- [Windows](https://docs.docker.com/docker-for-windows/install/)
- [Mac](https://docs.docker.com/docker-for-mac/install/)
- [Linux](https://runnable.com/docker/install-docker-on-linux)
- Verify docker is present by starting terminal or cmd and enter `docker --version`
- If old instalation of docker is present - [update](https://docs.docker.com/docker-for-windows/install/#updates) it.


## 2. Verify Docker Compose is present or install it (this step is needed for Linux OS)
- [Install Docker Compose](https://docs.docker.com/compose/install/)

## 3. Clone this [repository](https://gitlab.com/TelerikAcademy/alpha-28-qa/-/tree/master) or download the files from [here](https://gitlab.com/TelerikAcademy/alpha-28-qa/-/tree/master/05.%20Final%20Project/WEare%20Deploy%20ready).

## 4. Open *Final project*  folder and start terminal or cmd in the directory containing the `docker-compose.yml` file

## 5. Start/stop the application
- Run this command to start: `docker-compose up`
   - Images will be build/downloaded and start. If the terminal is closed - the app will stop!
- Run this command to start: `docker-compose up -d`
   - Images will be build/downloaded and started in containers in a detached mode (closing the terminal won't stop the application)
- Run this command to stop: `docker-compose down`

## 6. Verify the app is running
- Open [http://localhost:8081/](http://localhost:8081/) in a browser


