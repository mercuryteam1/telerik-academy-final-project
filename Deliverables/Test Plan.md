# Test Plan
## "WЕаre Social Network"

### Prepared by 'Team Mercurius':
- *Andrey Mihalev*
- *Manuela Petrova*

# Contents:
1. Purpose of the Test Plan
2. Software Overview
3. Scope
4. Approach and tools
5. Estimation
6. Exit criteria
7. Priority and Severity Tables
8. Environment


## 1. Purpose of the Test Plan
The purpose of this document is to describe the detailed test plan for the software under test - "WEare" social network. The application is web based network which helps people to find professional services provided by other people.

## 2. Software Overview
**["WEare"](http://164.138.216.247:8082)** is a website, connecting people in different areas of activity, offering their professional services.
The website contains the following functionalities and features:
- Register and Sign In
- Search for professional category and/or people
- Post creation
- Edit/Delete post
- Post/Like comments and posts
- Profile update


## 3. Scope
### 3.1. In Scope
   - Register
   - Sign In
   - Create/Like/Unlike/Delete a New Post
   - Create/Like/Unlike/Delete a Comment
   - Edit Profile:
        - Personal Information
        - Professional Information
        - Services
        - Settings
   - Search by profession and/or person

### 3.2. Out of Scope
   - Security tests
   - User Acceptance Test \(UAT)
   - Different user roles and managing settings
   - Tests outside of the test plan

## 4. Approach and tools
**Approach**
- The testing process will start with collecting the information about the website, creation of test cases, exploratory testing, performed by Mercurius team. After that will begin functionality testing of the in scoop main functionalities.

**Tools**

 - For creating test design model will be used Google sheets.
 - For issue logging and bug tracking will be used Gitlab.
 - API requests will be created through Postman.
 - Tests will be automated with Java and Selenium.

## 5. Estimation

#### The total time allotted for project testing is 30 days and is distributed as follows:

- 5 days for reading documentation, exploratory testing and creating Test plan.
- 5 days for creating Test case template, Git repository, Trello board and choose a Bug tracking tool.   
- 6 days for design and creation of the test cases and manual execution.
- 5 days for creating API requests with Postman.
- 7 days for automating high priority test cases.
- 2 days for creating reports and logging the issues.

## 6. Exit criteria

#### The testing process should stop when one of the following conditions is met:

 - 100% of the test cases are executed
 - 95% of the critical and major test cases passed
 - All of the critical defects are closed
 - Delivery of test report
 - All of the required documents are delivered
 - The budget is reached

## 7. Priority and Severity Tables

### 7.1. Priority

|Priority|Description|
|---|---|
|1 - Critical | The defects need to be fixed **immediately** |
|2 - High | The defects impact the website's main features |
|3 - Medium | The defects causes minimal deviation from the requirements |
|4 - Low | The defects have very minor affect to website's operation |


### 7.2. Severity

|Severity|Description|
|---|---|
|1 - Critical | The defect completely blocks testing of the product/feature |
|2 - Major | Not meeting its requirements and behaves differently than expected |
|3 - Moderate | Any feature implemented that is not meeting its requirements but the impact is negligible |
|4 - Low | Any cosmetic defects including spelling mistakes or alignment issues or font casing |



## 8. Environment


Environment 1

| | |
|---|---|
|OS name and version | macOS BigSur 11.4 |
|Model Name | MacBook Pro |
|Processor | 2,6 GHz Quad-Core Intel Core i7 |
|RAM | 16GB |
|Browser | Google Chrome 91.0.4472.114 (Official Build)   <br>Safari 14.1.1 |

Environment 2

| | |
|---|---|
|OS name and version | Windows 10 Home|
|Model Name | HUAWEI MATE D 14|
|Processor | AMD RYZEN 3500U |
|RAM | 8GB |
|Browser | Google Chrome 91.0.4472.114 (Official Build) |